/**
 * 
 */
package figury;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.util.Random;

/**
 * @author tb
 *
 */
public class Figura implements Runnable, ActionListener {

	// wspolny bufor
	protected Graphics2D FigureBuffer;
	protected Area FigureArea;
	// do wykreslania
	protected Shape FigureShape;
	// przeksztalcenie obiektu
	protected AffineTransform aft;

	// przesuniecie
	private int dx, dy;
	// rozciaganie
	private double sf;
	// kat obrotu
	private double an;
	
	private boolean beKilled;
	private boolean beTransparent;
	private boolean beRandom;
	private int delay;
	private int WindowWidth;
	private int WindowHeight;
	private Color clr;

	protected static final Random rand = new Random();

	private void randomColor() { 
		clr = new Color(rand.nextInt(255), rand.nextInt(255), rand.nextInt(255), rand.nextInt(255));;
	}
	public void Kill()
	{
		beKilled = true;
	} 
	
	public Figura(Graphics2D buf, int del, int w, int h) {
		delay = del;
		FigureBuffer = buf;
		WindowWidth = w;
		WindowHeight = h;
		dx = 1 + rand.nextInt(5);
		dy = 1 + rand.nextInt(5);
		sf = 1 + 0.05 * rand.nextDouble();
		an = 0.1 * rand.nextDouble();
		
		beTransparent = (rand.nextInt() % 17 == 0);
		beRandom = (rand.nextInt() % 17 == 0);
		clr = new Color(rand.nextInt(255), rand.nextInt(255), rand.nextInt(255), rand.nextInt(255));
		// reszta musi być zawarta w realizacji klasy Figure
		// (tworzenie figury i przygotowanie transformacji)

	}

	//@Override
	public void run() {
		
		// przesuniecie na srodek
		aft.translate(100, 100);
		FigureArea.transform(aft);
		FigureShape = FigureArea;

		while (!beKilled) {
				
			// przygotowanie nastepnego kadru
			
			if(!beKilled)
				if(AnimPanel.getState())
					FigureShape = nextFrame();
			
			try {
				Thread.sleep(delay);
			} catch (InterruptedException e) {
			}
		}
		
		System.gc();
		System.out.println("Figure has done its job. " + this.toString());
	}

	protected Shape nextFrame() {
		// zapamietanie na zmiennej tymczasowej
		// aby nie przeszkadzalo w wykreslaniu
		
		if(beKilled)
			return null;
		FigureArea = new Area(FigureArea); 
		aft = new AffineTransform();
		Rectangle FigurePos = FigureArea.getBounds();	
		
		int FPosCenterX = FigurePos.x + FigurePos.width / 2;
		int FPosCenterY = FigurePos.y + FigurePos.height / 2;
		
		// odbicie
		if (FigurePos.x < 0 || FigurePos.x + FigurePos.width > WindowWidth)
			dx = -dx; 
		if (FigurePos.y < 0 || FigurePos.y + FigurePos.height > WindowHeight)
			dy = -dy;
		
		
		// zwiekszenie lub zmniejszenie
		if (FigurePos.height > WindowHeight / 3 || FigurePos.height < 10)
			sf = 1 / sf;
		// konstrukcja przeksztalcenia
		
		if((FigurePos.x >= 0 || FigurePos.x + FigurePos.width != WindowWidth) && 
				(FigurePos.y >= 0 || FigurePos.y + FigurePos.height != WindowHeight)) {
				aft.translate(FPosCenterX, FPosCenterY);
		
				aft.scale(sf, sf);
				aft.rotate(an);
				aft.translate(-FPosCenterX, -FPosCenterY);
		}
		
		aft.translate(dx, dy);
		// przeksztalcenie obiektu
		FigureArea.transform(aft);
		return FigureArea;
	}

	//@Override
	public void actionPerformed(ActionEvent evt) {
		// wypelnienie obiektu
		
		if(beKilled)
			return;
		
		if(beRandom && !beTransparent)
			randomColor();
		
		if(!beTransparent)
		{	
			FigureBuffer.setColor(clr.brighter());
			FigureBuffer.fill(FigureShape);
		}
		
		// wykreslenie ramki
		FigureBuffer.setColor(clr.darker());
		FigureBuffer.draw(FigureShape);
	}

}

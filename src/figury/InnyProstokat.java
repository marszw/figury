package figury;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.RoundRectangle2D;

public class InnyProstokat extends Figura{

	public InnyProstokat(Graphics2D buf, int del, int w, int h) {
		super(buf, del, w, h);
		// TODO Auto-generated constructor stub
		
		FigureShape =  new RoundRectangle2D.Float(0,0,20,20, 10, 5);
		FigureArea = new Area(FigureShape);
		aft = new AffineTransform();

	}
	
}
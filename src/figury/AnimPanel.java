package figury;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JPanel;
import javax.swing.Timer;

public class AnimPanel extends JPanel implements ActionListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// bufor
	ArrayList <Figura> FigureList = new ArrayList<Figura>();
	Image image;
	// wykreslacz ekranowy
	Graphics2D device;
	// wykreslacz bufora
	Graphics2D buffer;

	private int delay = 20;

	private Timer timer;
	static private boolean isRunning = false;
	//private static int numer = 0;
	static public boolean getState()
	{
		return isRunning;
	}
	static	public void setState(boolean State)
	{
		isRunning = State;
	}
	
	public AnimPanel() {
		super();
		setBackground(Color.WHITE);
		timer = new Timer(delay, this);
	}

	 
	public void refresh() 
	{
		setPreferredSize(null);
		initialize();
		
		for ( int i = 0; i < FigureList.size() ; i++) {
			FigureList.get(i).Kill();
			FigureList.remove(i);
		}

		
	}   
	
	public void initialize() {
		
		int width = getWidth();
		int height = getHeight();

		image = createImage(width, height);
		buffer = (Graphics2D) image.getGraphics();
		buffer.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		device = (Graphics2D) getGraphics();
		device.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	}

	void addFig() {
		Figura fig;
		int iRand = (new Random()).nextInt(); 
		
		if( iRand % 3 == 0)
			fig = new Kwadrat(buffer, delay, getWidth(), getHeight());
		else if ( iRand % 3 == 1) 
			fig = new Elipsa(buffer, delay, getWidth(), getHeight());
		else
			fig = new InnyProstokat(buffer, delay, getWidth(), getHeight());
			
		FigureList.add(fig);
		timer.addActionListener(fig);
		new Thread(fig).start();
	}

	void animate() {
			if (timer.isRunning()) {
				timer.stop();
			} else {
				timer.start();
			}
	}

	public void actionPerformed(ActionEvent e) {
		device.drawImage(image, 0, 0, null);
		buffer.clearRect(0, 0, getWidth(), getHeight());
	}
}
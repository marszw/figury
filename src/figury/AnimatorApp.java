package figury;
/* Kolejne figury
Zapisywanie i odtwarzanie animacji dzieki 2. przyciskowi
Obszar Animacji powiekszany na caly ekran
Animacja przezroczysta
Poprawienie programu zeby animacje nie wychodzily poza okno

Stworzenie klasy kwadrat oraz elipsa
*/

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Frame;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowStateListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.Timer;




public class AnimatorApp extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					final AnimatorApp frame = new AnimatorApp();
					frame.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @param delay 
	 */
	
	public AnimatorApp() {
		
		
		//dispose();

		setLayout(new BorderLayout());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		int ww = 450, wh = 300;
		setBounds((screen.width-ww)/2, (screen.height-wh)/2, ww, wh);
		contentPane = new JPanel();
		contentPane.setLayout(new BorderLayout());
		setContentPane(contentPane);
//		contentPane.setLayout(null);
		
		final AnimPanel kanwa = new AnimPanel();
		//kanwa.setBounds(10, 11, 422, 219);
		
		contentPane.add(kanwa, BorderLayout.CENTER);
		SwingUtilities.invokeLater(new Runnable() {
			
			public void run() {
				kanwa.initialize();
			}
		});

		final JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				kanwa.addFig();
			}
		});
		
		btnAdd.setBounds(10, 239, 80, 23);
		btnAdd.setEnabled(AnimPanel.getState());
		contentPane.add(btnAdd, BorderLayout.PAGE_END);
		
		JButton btnAnimate = new JButton("Animate");
		btnAnimate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AnimPanel.setState(!AnimPanel.getState());
				btnAdd.setEnabled(AnimPanel.getState());
				kanwa.animate();
			}
		});
		btnAnimate.setBounds(100, 239, 180, 23);
		contentPane.add(btnAnimate, BorderLayout.EAST);
		//DOWOLNY ROZMIAR 
		
		Timer TRSize = new Timer(25, new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					
				Dimension Wind = getSize();
					try {
						Thread.sleep(10);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					if(!Wind.equals(getSize()))
					{
						revalidate();
						kanwa.refresh();
						contentPane.repaint();				
					
					}
					
			}} );

		
		TRSize.setRepeats(true);
		TRSize.start();
		
		addWindowStateListener(new WindowStateListener() {

			public void windowStateChanged(WindowEvent e) {
				revalidate();
					// TODO Auto-generated method stub
				if( (e.getNewState() & Frame.MAXIMIZED_BOTH) ==  Frame.MAXIMIZED_BOTH )
				{
					kanwa.refresh();
					contentPane.repaint();	
				
				}
				else if (( e.getNewState() & Frame.MAXIMIZED_BOTH) != Frame.MAXIMIZED_BOTH)
				{
					kanwa.refresh();
					contentPane.repaint();	
				}
			
				
			} 
			
			
		});
		
	}

}